# README #

This is a micro-application with one function: to retrieve the serial number of an attached Magtek Dynamag credit card reader, of the type used in the VE Kiosk.  The button opens a File dialog, through which the user can choose the location and filename of a .txt file containing the retrieved serial number.

## Troubleshooting ##

### I don't see a serial number here.  What gives? ###
This means the app couldn't retrieve the serial number when it launched.  Either the credit card reader is not connected, or there's another application holding the connection to the credit card reader.  KioskUI.exe is the most likely culprit.  Close the app, check your connections, shut down KioskUI, and give it another go.