﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MTSCRANET;
using System.IO;

namespace SerialPlz
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public class Scanner
    {
        private MTSCRANET.MTSCRA reader;
        public Scanner()
        {
            ConfigureDevice();
        }
        private string _deviceSerial;
        public string DeviceSerial
        {
            get
            {
                return _deviceSerial;
            }
            set
            {
                _deviceSerial = value;
            }
        }
        public void ConfigureDevice()
        {
            reader = new MTSCRA();
            reader.setConnectionType(MTLIB.MTConnectionType.USB);
            reader.openDevice();
            DeviceSerial = GetDeviceSerial();
        }
        private string GetDeviceSerial()
        {
            string prefix = "K3MT";
            string srl = reader.getDeviceSerial();
            if (string.IsNullOrEmpty(srl))
            {
                return string.Empty;
            }
            else
            {
                return prefix + srl;
            }
        }
    }
    public partial class MainWindow : Window
    {
        Scanner scn;
        public MainWindow()
        {
            InitializeComponent();
            scn = new Scanner();
            tbSerial.Text = scn.DeviceSerial;
        }

        private void btnSaveSerial_Click(object sender, RoutedEventArgs e)
        {
            String srl;
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "serial";
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt)|*.txt";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                StreamWriter sw = new StreamWriter(dlg.FileName);
                srl = scn.DeviceSerial;
                sw.WriteLine(srl);
                sw.Close();
            }
        }
    }
}
